import React, { Component } from "react";

class GheItem extends Component {
  state = {
    isBooking: false,
  };

  render() {
    const { seatItem, handleBooked } = this.props;
    let style = { height: 40, width: 40, marginBottom: 20 };
    return (
      <div className="container">
        {seatItem.TrangThai === true ? (
          <button
            style={{ ...style, cursor: "not-allowed" }}
            disabled="disabled"
            className="btn btn-danger"
          >
            {seatItem.SoGhe}
          </button>
        ) : (
          <button
            onClick={() => {
              handleBooked(seatItem);
              {
                !this.state.isBooking
                  ? this.setState({
                      isBooking: true,
                    })
                  : this.setState({
                      isBooking: false,
                    });
              }
            }}
            style={style}
            className={
              this.state.isBooking ? "btn btn-success" : "btn btn-dark"
            }
          >
            {seatItem.SoGhe}
          </button>
        )}
      </div>
    );
  }
}

export default GheItem;
