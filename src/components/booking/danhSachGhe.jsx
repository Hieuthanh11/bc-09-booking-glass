import React, { Component } from "react";
import GheItem from "./gheItem";

class DanhSachGhe extends Component {
  renderSeatList = () => {
    const { seatList, handleBooked } = this.props;
    return seatList.map((item, index) => {
      return (
        <div key={index} className="col-3">
          <GheItem seatItem={item} handleBooked={handleBooked} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="row text-center">
        <div
          style={{ backgroundColor: "#CFCFCF" }}
          className="col-12 py-2 mb-3"
        >
          Tài xế
        </div>
        {this.renderSeatList()}
      </div>
    );
  }
}

export default DanhSachGhe;
