import React, { Component } from "react";

class DanhSachGheDangDat extends Component {
  renderChoosen = () => {
    const { booked } = this.props;
    return booked.map((item, index) => {
      return (
        <tr key={index}>
          <td>Ghế</td>
          <td>Số:{item.seat.SoGhe}</td>
          <td>$:{item.seat.Gia}</td>
          <td>
            <button
              className="btn btn-dark"
              disabled="disabled"
              style={{ cursor: "not-allowed" }}
            >
              Hủy
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="table">
        <tbody>{this.renderChoosen()}</tbody>
      </table>
    );
  }
}

export default DanhSachGheDangDat;
