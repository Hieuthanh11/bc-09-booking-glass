import React, { Component } from "react";

class GlassItem extends Component {
  render() {
    const { itemGlass, handleFit } = this.props;
    return (
      <button onClick={() => handleFit(itemGlass)} className="mx-1">
        <img style={{ width: 100, height: 50 }} src={itemGlass.url} />
      </button>
    );
  }
}

export default GlassItem;
