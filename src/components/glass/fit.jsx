import React, { Component } from "react";
import Model from "./img/model.jpg";

class Fit extends Component {
  render() {
    const { selectedGlass } = this.props;
    return (
      <div
        style={{ position: "relative", zIndex: 10 }}
        className="container w-25 "
      >
        <img className="w-75" src={Model} alt="model" />
        <img
          style={{
            height: 40,
            width: 120,
            position: "absolute",
            top: 68,
            left: 65,
            opacity: 0.7,
          }}
          src={selectedGlass.url}
        />
      </div>
    );
  }
}

export default Fit;
