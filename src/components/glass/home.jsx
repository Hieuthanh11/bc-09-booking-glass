import React, { Component } from "react";
import Fit from "./fit";
import GlassList from "./glassList";
import "./home.css";
import Glass1 from "./img/v1.png";
import Glass2 from "./img/v2.png";
import Glass3 from "./img/v3.png";
import Glass4 from "./img/v4.png";
import Glass5 from "./img/v5.png";
import Glass6 from "./img/v6.png";
import Glass7 from "./img/v7.png";
import Glass8 from "./img/v8.png";
import Glass9 from "./img/v9.png";

class Home extends Component {
  arrProduct = [
    {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: Glass1,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 2,
      price: 50,
      name: "GUCCI G8759H",
      url: Glass2,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 3,
      price: 30,
      name: "DIOR D6700HQ",
      url: Glass3,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 4,
      price: 30,
      name: "DIOR D6005U",
      url: Glass4,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 5,
      price: 30,
      name: "PRADA P8750",
      url: Glass5,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 6,
      price: 30,
      name: "PRADA P9700",
      url: Glass6,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 7,
      price: 30,
      name: "FENDI F8750",
      url: Glass7,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 8,
      price: 30,
      name: "FENDI F8500",
      url: Glass8,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
    {
      id: 9,
      price: 30,
      name: "FENDI F4300",
      url: Glass9,
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  ];
  state = {
    selectedGlass: [],
  };

  handleFit = (glass) => {
    this.setState({
      selectedGlass: glass,
    });
  };
  render() {
    return (
      <div className="index">
        <header className="bg-dark py-3"></header>
        <h3
          style={{
            backgroundColor: "rgba(0, 0, 0, 0.411)",
            zIndex: 10,
            position: "relative",
          }}
          className="py-4 text-white text-center"
        >
          TRY GLASSES APP ONLINE
        </h3>

        <Fit selectedGlass={this.state.selectedGlass} />

        <GlassList arrProduct={this.arrProduct} handleFit={this.handleFit} />
      </div>
    );
  }
}

export default Home;
