import React, { Component } from "react";
import GlassItem from "./glassItem";

class GlassList extends Component {
  renderGlasses = () => {
    const { arrProduct, handleFit } = this.props;
    return arrProduct.map((item, index) => {
      return (
        <div key={index}>
          <GlassItem itemGlass={item} handleFit={handleFit} />
        </div>
      );
    });
  };
  render() {
    return (
      <div
        style={{ position: "relative", zIndex: 20 }}
        className="container mt-5"
      >
        <div className="row">{this.renderGlasses()}</div>
      </div>
    );
  }
}

export default GlassList;
